//
//  UserDefaultsExtension.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 16.12.21..
//  Copyright © 2021 Adhoc Technologies. All rights reserved.
//

import Foundation
import UIKit
import AdSupport

let KEY_SESSION_ID:String = "SessionID"
let KEY_REQUEST_ID:String = "RequestID"
let KEY_SESSION_VIEW_ID:String = "ViewID"
let KEY_CONTENT_SESSION_ID:String = "ContentSessionID"
let KEY_TRACKING_AUTHORIZATION:String = "TrackingAuthorisation"

public extension UserDefaults {
    enum UserDefaultsKeys : String {
        case trackingAuthorization
        case trackingAuthorizationOptOut
        case userRequestID
        case userSessionID
        case userSessionViewID
        case userContentSessionId
        case userAdSessionID
    }
    
    //MARK: Set Tracking Authorization
    func setTrackingAuthorization(value: Bool) {
        let oldValue = isTrackingAuthorized()
        if value == true {
            set(false,forKey: UserDefaultsKeys.trackingAuthorizationOptOut.rawValue)
        } else if value == false && oldValue == true {
            set(true,forKey: UserDefaultsKeys.trackingAuthorizationOptOut.rawValue)
        }
        set(value, forKey: UserDefaultsKeys.trackingAuthorization.rawValue)
        //synchronize()
    }

    //MARK: Check Tracking Authorization
    func isTrackingAuthorized()-> Bool {
        return bool(forKey: UserDefaultsKeys.trackingAuthorization.rawValue)
    }

    func isTrackingAuthorizationOptOut()-> Bool {
        return bool(forKey: UserDefaultsKeys.trackingAuthorizationOptOut.rawValue)
    }
    
    
    //MARK: Create User RequestID
    func createUserRequestID(){
        let uuid = UUID().uuidString.lowercased()
        set(uuid, forKey: UserDefaultsKeys.userRequestID.rawValue)
    }

    //MARK: Set User RequestID
    func setUserRequestID(uuid: String){
        set(uuid.lowercased(), forKey: UserDefaultsKeys.userRequestID.rawValue)
    }
    
    //MARK: Clear User RequestID
    func clearUserRequestID(){
        removeObject(forKey: UserDefaultsKeys.userRequestID.rawValue)
    }
    
    //MARK: Retrieve User RequestID
    func getUserRequestID() -> String {
        guard let uuid = string(forKey: UserDefaultsKeys.userRequestID.rawValue) else {
            let uuid = UUID().uuidString.lowercased()
            set(uuid, forKey: UserDefaultsKeys.userRequestID.rawValue)
            return uuid
        }
        return uuid
    }
    
    
    
    //MARK: Create User SessionID
    @objc func createUserSessionID(){
        let uuid = UUID().uuidString.lowercased()
        set(uuid, forKey: UserDefaultsKeys.userSessionID.rawValue)
    }
    
    //MARK: Set User SessionID
    @objc func setUserSessionID(uuid: String){
        set(uuid.lowercased(), forKey: UserDefaultsKeys.userSessionID.rawValue)
    }
    
    //MARK: Clear User SessionID
    @objc func clearUserSessionID(){
        removeObject(forKey: UserDefaultsKeys.userSessionID.rawValue)
    }

    //MARK: Retrieve User SessionID
    func getUserSessionID() -> String {
        guard let uuid = string(forKey: UserDefaultsKeys.userSessionID.rawValue) else {
            let uuid = UUID().uuidString.lowercased()
            set(uuid, forKey: UserDefaultsKeys.userSessionID.rawValue)
            return uuid
        }
        return uuid
    }
    
    
    
    //MARK: Create User SessionViewID
    func createUserSessionViewID(){
        let uuid = UUID().uuidString.lowercased()
        set(uuid, forKey: UserDefaultsKeys.userSessionViewID.rawValue)
    }
    
    //MARK: Set User SessionViewID
    func setUserSessionViewID(uuid: String){
        set(uuid.lowercased(), forKey: UserDefaultsKeys.userSessionViewID.rawValue)
    }
    
    //MARK: Clear User SessionViewID
    func clearUserSessionViewID(){
        removeObject(forKey: UserDefaultsKeys.userSessionViewID.rawValue)
    }

    //MARK: Retrieve User SessionViewID
    func getUserSessionViewID() -> String {
        guard let uuid = string(forKey: UserDefaultsKeys.userSessionViewID.rawValue) else {
            let uuid = UUID().uuidString.lowercased()
            set(uuid, forKey: UserDefaultsKeys.userSessionViewID.rawValue)
            return uuid
        }
        return uuid
    }
    
    
    
    //MARK: Create User Conten SessionID
    func createUserContentSessionID(){
        let uuid = UUID().uuidString.lowercased()
        set(uuid, forKey: UserDefaultsKeys.userContentSessionId.rawValue)
    }

    //MARK: Set User RequestID
    func setUserContentSessionID(uuid: String){
        set(uuid.lowercased(), forKey: UserDefaultsKeys.userContentSessionId.rawValue)
    }
    
    //MARK: Clear User RequestID
    func clearUserContentSessionID(){
        removeObject(forKey: UserDefaultsKeys.userContentSessionId.rawValue)
    }
    
    //MARK: Retrieve User RequestID
    func getUserContentSessionID() -> String {
        guard let uuid = string(forKey: UserDefaultsKeys.userContentSessionId.rawValue) else {
            let uuid = ""
            return uuid
        }
        return uuid
    }
    
    
    
    
    //MARK: Create User AdSessionID
    func createUserAdSessionID(){
        let uuid = UUID().uuidString.lowercased()
        set(uuid, forKey: UserDefaultsKeys.userAdSessionID.rawValue)
    }
    
    //MARK: Set User AdSessionID
    func setUserAdSessionID(adId: String){
        set(adId, forKey: UserDefaultsKeys.userAdSessionID.rawValue)
    }

    //MARK: Clear User AdSessionID
    func clearUserAdSessionID(){
        removeObject(forKey: UserDefaultsKeys.userAdSessionID.rawValue)
    }
    
    //MARK: Retrieve User AdSessionID
    func getUserAdSessionID() -> String {
        guard let uuid = string(forKey: UserDefaultsKeys.userAdSessionID.rawValue) else {
            let uuid = UUID().uuidString.lowercased()
            set(uuid, forKey: UserDefaultsKeys.userAdSessionID.rawValue)
            return uuid
        }
        return uuid
    }

}

//
//  FloatingPointExtensions.swift
//  DZ_Adapter_tvOS_Native
//
//  Created by Vuk on 19.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public extension Double {
    @inlinable
    func signum( ) -> Self {
        if self < 0 { return -1 }
        if self > 0 { return 1 }
        return 0
    }
}

//
//  DZCollector.swift
//  DataZoom
//
//  Created by Momcilo Stankovic on 05/04/21.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer
import AVKit
import Network
import AdSupport
import Reachability

#if canImport(AppTrackingTransparency)
import AppTrackingTransparency
#endif

public protocol DZConnectorProtocol {
    func onCreateAppSessionID()
    func onResetAppSessionID()
    
    func onCreateContentSessionID()
    func onResetContentSessionID()
}

@objc open class DZConnector : NSObject {
    public let reachability = try! Reachability()
    
    public var connectorDelegate: DZConnectorProtocol?
    
    public var presentTime : Float64 = 0.0
    public var prevTime : Float64 = 0.0
    
    public var stopped = false
    public var timeObserverToken: AnyObject?
    public var periodicObserverToken: AnyObject?
    
    public var idfa: UUID?
    
    public var fluxDataFlag = true
    public var flagForAutomationOnly = Bool()

    @objc open func initialise(withConfigId: String, andURL:String, onCompletion: @escaping (Bool,Error?) -> Void)  {
        
        UserDefaults.standard.createUserSessionID()
        connectorDelegate?.onCreateAppSessionID()
        
        if reachability.connection != .unavailable {
            DZWebBroker.shared.initialise(configId: withConfigId, connectURL: andURL) { (sucess, error) in
                print("Initialize with config done")
                if sucess == true {
                    DZEventCollector.shared.fluxDataFlag = self.fluxDataFlag
                    
                    DZWebBroker.shared.getNetworkDetails(nil, onSuccess: { (success) in
                        DZEventCollector.shared.initWithConfiguration()
                        onCompletion(true,nil)
                    })

                    if self.flagForAutomationOnly == true {
                        DZEventCollector.shared.flagForAutomationOnly = true

                        DZWebBroker.shared.getRunIDForAutomation { (success, error) in
                            if success == true {
                                print("Get run id")

                                onCompletion(true,nil)
                            } else {
                                print("run id error >>>>\(String(describing: error?.localizedDescription))")
                                onCompletion(false,error!)
                            }
                        }
                    }
                } else {
                    //self.removeTimer()
                    onCompletion(false,error!)
                }
            }
        } else {
            print("error")
        }
    }
}

extension DZConnector {
    public func askConsent() {
        if #available(iOS 14, macOS 11.0, tvOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                DispatchQueue.main.async {
                    switch status {
                    case .authorized:
                        // Authorized
                        UserDefaults.standard.setTrackingAuthorization(value: true)
                        
                        self.idfa = ASIdentifierManager.shared().advertisingIdentifier
                        print(self.idfa?.uuidString ?? "No IDFA")

                    case .denied,
                         .notDetermined,
                         .restricted:
                        UserDefaults.standard.setTrackingAuthorization(value: false)
                        break
                        
                    @unknown default:
                        break
                    }
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

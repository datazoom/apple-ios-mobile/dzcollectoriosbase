//
//  PlayerState.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public enum PlayerState {
    case idle
    
    case contentError
    
    case contentRequested
    case contentBuffering
    case contentPlaying
    case contentPaused
    case contentSeeking
    case contentSeeked
    case contentCompleted
    case contentSkipped

    case adBreakStarted
    case adBreakEnded
    
    case adError

    case adRequested
    case adBuffering
    case adPlaying
    case adPaused
    case adSeeking
    case adSeeked
    case adCompleted
    case adSkipped
    
    
    case request
    case buffering
    case playing
    case paused
    case completed
    
    static let defaultType: Self = .idle
    
    public var state: String {
        switch self {
        case .idle:
            return "IDLE"
            
        case .contentError:
            return "CT_ERROR"
        case .contentRequested:
            return "CT_REQUESTED"
        case .contentBuffering:
            return "CT_BUFFERING"
        case .contentPlaying:
            return "CT_PLAYING"
        case .contentPaused:
            return "CT_PAUSED"
        case .contentSeeking:
            return "CT_SEEKING"
        case .contentSeeked:
            return "CT_SEEKED"
        case .contentCompleted:
            return "CT_COMPLETED"
        case .contentSkipped:
            return "CT_SKIPPED"

        case .adBreakStarted:
            return "AD_BREAK_STARTED"
        case .adBreakEnded:
            return "AD_BREAK_ENDED"
        
        case .adError:
            return "AD_ERROR"
        case .adRequested:
            return "AD_REQUESTED"
        case .adBuffering:
            return "AD_BUFFERING"
        case .adPlaying:
            return "AD_PLAYING"
        case .adPaused:
            return "AD_PAUSED"
        case .adSeeking:
            return "AD_SEEKING"
        case .adSeeked:
            return "AD_SEEKED"
        case .adCompleted:
            return "AD_COMPLETED"
        case .adSkipped:
            return "AD_SKIPPED"
            
            
            
        case .request:
            return "request"
        case .buffering:
            return "buffering"
        case .playing:
            return "playing"
        case .paused:
            return "paused"
        case .completed:
            return "completed"
        }
    }
}

extension PlayerState : Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.state)
    }
}

extension PlayerState : Identifiable {
    public var id: Self { self }
}

extension PlayerState : Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.hashValue == rhs.hashValue
      }
}

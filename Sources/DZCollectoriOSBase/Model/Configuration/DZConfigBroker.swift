//
//  BrokerUrl.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 Adhoc Technologies. All rights reserved.
//

import Foundation

public struct DZConfigBroker {
    let url: String?
}

extension DZConfigBroker : Decodable {
    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decodeIfPresent(String.self, forKey: .url)
    }
}

//
//  DZConfigTypeElement.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.2.22..
//  Copyright © 2022 Adhoc Technologies. All rights reserved.
//

import Foundation

//MARK:- Types Element
public struct DZConfigTypeElement {
    public let name : String?
}

extension DZConfigTypeElement : Decodable {
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
    
    public init(_ name: String) {
        self.name = name
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}



public struct DZConfigTypeElementV3 {
    public let name : String?
    public let mediaTypes : [String]?
}

extension DZConfigTypeElementV3 : Decodable {
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case mediaTypes = "media_types"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        mediaTypes = try values.decodeIfPresent([String].self, forKey: .mediaTypes)
    }
}

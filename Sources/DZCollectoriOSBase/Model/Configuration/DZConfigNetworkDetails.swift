//
//  NetworkDetails.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

//MARK:- Network Details
public struct DZConfigNetworkDetails {
    let asn : String?
    let city : String?
    let country : String?
    let countryCode : String?
    let district : String?
    let isp : String?
    let latitude : Double?
    let longitude : Double?
    var org : String?
    let query : String?
    let region : String?
    let regionName : String?
    let status : String?
    let timezone : String?
    let timezoneOffset : Float?
    let zip : String?
    let continent : String?
    let continentCode : String?
    let mobileConnection : Bool?
}

extension DZConfigNetworkDetails : Decodable {
    enum CodingKeys: String, CodingKey {        
        case asn = "as"
        case city = "city"
        case country = "country"
        case countryCode = "countryCode"
        case district = "district"
        case isp = "isp"
        case latitude = "lat"
        case longitude = "lon"
        case org = "org"
        case query = "query"
        case region = "region"
        case regionName = "regionName"
        case status = "status"
        case timezone = "timezone"
        case timezoneOffset = "offset"
        case zip = "zip"
        case continent = "continent"
        case continentCode = "continentCode"
        case mobileConnection = "mobile"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        asn = try values.decodeIfPresent(String.self, forKey: .asn)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        isp = try values.decodeIfPresent(String.self, forKey: .isp)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
        org = try values.decodeIfPresent(String.self, forKey: .org)
        query = try values.decodeIfPresent(String.self, forKey: .query)
        region = try values.decodeIfPresent(String.self, forKey: .region)
        regionName = try values.decodeIfPresent(String.self, forKey: .regionName)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        timezoneOffset = try values.decodeIfPresent(Float.self, forKey: .timezoneOffset)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
        continent = try values.decodeIfPresent(String.self, forKey: .continent)
        continentCode = try values.decodeIfPresent(String.self, forKey: .continentCode)
        mobileConnection = try values.decodeIfPresent(Bool.self, forKey: .mobileConnection)
        
        if org == nil || ((org?.isEmpty) != nil) {
            org = isp
        }
    }
}

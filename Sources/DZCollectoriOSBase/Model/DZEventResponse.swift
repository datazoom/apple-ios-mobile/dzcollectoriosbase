//
//  DZEventResponse.swift
//  GoldCollectorTvOS
//
//  Created by Momcilo Stankovic on 03/12/2020.
//  Copyright © 2020 RedCellApps. All rights reserved.
//


import Foundation

enum DZEventResponseError: Error {
    case notFound

    // Throw in all other cases
    case unexpected(code: Int)
}


extension DZEventResponseError: LocalizedError {
    public var errorDescription: String {
        switch self {
        case .notFound:
            return "The specified item could not be found."
        case .unexpected(_):
            return "An unexpected error occurred."
        }
    }
}

class ItemResponse: Codable {
    var event_id: String?
    var status: String?
    var status_code: HTTPStatusCode?
//    var error: Error?
}

class DZEventResponse: Codable, CustomStringConvertible {
    var status: String?
    var status_code: Int?
    var message: String?
    var has_errors: Bool?
    var item_count: Int?
    var items: [ItemResponse] = []
    
    init(response: DZEventResponse) {
      status = response.status
      status_code = response.status_code
      message = response.message
      has_errors = response.has_errors
      item_count = response.item_count
      items = response.items
    }
    
    required init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      status = try values.decode(String.self, forKey: .status)
      status_code = try values.decode(Int.self, forKey: .status_code)
      message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
      has_errors = try values.decode(Bool.self, forKey: .has_errors)
      item_count = try values.decode(Int.self, forKey: .item_count)
      items = try values.decode([ItemResponse].self, forKey: .items)
    }
    
    convenience init?(data: Data) {
      guard let response = DZEventResponse.make(data: data) else { return nil }
      self.init(response: response)
    }
    
    var description: String {
      return """
      status = \(status)
      status_code = \(status_code)
      message = \(message)
      has_errors = \(has_errors)
      items = \(items)
      """
    }
    
    private static func make(data: Data) -> DZEventResponse?  {
      return try? JSONDecoder().decode(DZEventResponse.self, from: data)
    }
}


enum HTTPStatusCode: Int, Error, Codable {
    case ok = 200
    case badRequest = 400
    case internalServerError = 500
    case serviceUnavailable = 503
    
    var isRetriable: Bool {
        return self == .serviceUnavailable
    }
}

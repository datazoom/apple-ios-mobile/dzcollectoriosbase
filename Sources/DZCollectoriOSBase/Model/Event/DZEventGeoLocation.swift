//
//  DZEventGeoLocation.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventGeoLocation {
    public var city : String
    public var country : String
    public var countryCode : String
    public var district : String
    public var latitude : Double
    public var longitude : Double
    public var regionName : String
    public var regionCode : String
    public var postalCode : String?
    public var continent : String
    public var continentCode : String
    public var timezoneName : String
    public var timezoneOffset : Float
    public var mobileConnection : String
    
    var configuration: [CodingKeys] = []
    
    public init() {
        self.city = ""
        self.country = ""
        self.countryCode = ""
        self.district = ""
        self.latitude = 0
        self.longitude  = 0
        self.regionCode = ""
        self.regionName = ""
        self.postalCode = ""
        self.continent = ""
        self.continentCode = ""
        self.timezoneName = ""
        self.timezoneOffset = 0.0
        self.mobileConnection = ""
    }
    
    public func initFromConfiguration(_ net: DZConfigNetworkDetails?) {
        if let geo = net {
            self.city = geo.city ?? ""
            self.country = geo.country ?? ""
            self.countryCode = geo.countryCode ?? ""
            self.district = geo.district ?? ""
            self.latitude = geo.latitude ?? 0
            self.longitude = geo.longitude ?? 0
            self.regionCode = geo.region ?? ""
            self.regionName = geo.regionName ?? ""
            self.postalCode = geo.zip ?? ""
            self.continent = geo.continent ?? ""
            self.continentCode = geo.continentCode ?? ""
            self.timezoneName = geo.timezone ?? ""
            var tzOffset = (geo.timezoneOffset != nil) ? (geo.timezoneOffset! / 3600.0) : 0.0
            if abs(tzOffset) > 24.0 {
                tzOffset = (tzOffset < 0.0 ? -1 : 1) * 24.0
            }
            self.timezoneOffset = tzOffset
            self.mobileConnection = (geo.mobileConnection == true) ? "true" : "false"
        }
    }
}

extension DZEventGeoLocation : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case city = "city"
        case country = "country"
        case countryCode = "country_code"
        case district = "district"
        case latitude = "latitude"
        case longitude = "longitude"
        case regionCode = "region_code"
        case regionName = "region"
        case postalCode = "postal_code"
        case continent = "continent"
        case continentCode = "continent_code"
        case timezoneName = "timezone_name"
        case timezoneOffset = "timezone_offset"
        case mobileConnection = "mobile_connection"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.city) {
            try container.encode(city, forKey: .city)
        }
        if configuration.contains(.country) {
            try container.encode(country, forKey: .country)
        }
        if configuration.contains(.countryCode) {
            try container.encode(countryCode, forKey: .countryCode)
        }
        if configuration.contains(.district) {
            try container.encode(district, forKey: .district)
        }
        if configuration.contains(.latitude) {
            try container.encode(latitude, forKey: .latitude)
        }
        if configuration.contains(.longitude) {
            try container.encode(longitude, forKey: .longitude)
        }
        if configuration.contains(.regionName) {
            try container.encode(regionName, forKey: .regionName)
        }
        if configuration.contains(.regionCode) {
            try container.encode(regionCode, forKey: .regionCode)
        }
        if configuration.contains(.postalCode) {
            try container.encode(postalCode, forKey: .postalCode)
        }
        
        if configuration.contains(.continent) {
            try container.encode(continent, forKey: .continent)
        }
        if configuration.contains(.continentCode) {
            try container.encode(continentCode, forKey: .continentCode)
        }
        if configuration.contains(.timezoneName) {
            try container.encode(timezoneName, forKey: .timezoneName)
        }
        if configuration.contains(.timezoneOffset) {
            try container.encode(timezoneOffset, forKey: .timezoneOffset)
        }
        if configuration.contains(.mobileConnection) {
            try container.encode(mobileConnection, forKey: .mobileConnection)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}

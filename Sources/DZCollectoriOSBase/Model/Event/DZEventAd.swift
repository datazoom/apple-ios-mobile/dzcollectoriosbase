//
//  DZEventAd.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventAd {
    public var adSessionId: String = String()
    public var adBrakeId: String = String()
    public var adId: String = String()
    public var adPosition: Int = Int()
    public var adSystem: String = String()
//    public var adWrapperSystem: DZEventAdWrapperSystem = DZEventAdWrapperSystem()
    public var adWrapperSystem: [String] = []
    
    var configuration: [CodingKeys] = []
}

extension DZEventAd : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case adSessionId = "ad_session_id"
        case adBrakeId = "ad_brake_id"
        case adId = "ad_id"
        case adPosition = "ad_position"
        case adSystem = "ad_system"
        case adWrapperSystem = "ad_wrapper_system"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.adSessionId) {
            try container.encode(adSessionId, forKey: .adSessionId)
        }
        if configuration.contains(.adBrakeId) {
            try container.encode(adBrakeId, forKey: .adBrakeId)
        }
        if configuration.contains(.adId) {
            try container.encode(adId, forKey: .adId)
        }
        if configuration.contains(.adPosition) {
            try container.encode(adPosition, forKey: .adPosition)
        }
        if configuration.contains(.adSystem) {
            try container.encode(adSystem, forKey: .adSystem)
        }
        if configuration.contains(.adWrapperSystem) {
            try container.encode(adWrapperSystem, forKey: .adWrapperSystem)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}

public class DZEventAdWrapperSystem {
}

extension DZEventAdWrapperSystem : Encodable {
//    enum CodingKeys: String, CodingKey {
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//    }
}

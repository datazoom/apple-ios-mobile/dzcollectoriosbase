//
//  DZEventDetailsMetrics.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 8.2.22..
//  Copyright © 2022 Adhoc Technologies. All rights reserved.
//

import Foundation

public class DZEventDetailsMetrics {
    var type: EventType = EventType.none
    
    // counts and numbers
    public var eventCount: Int = 0
    public var numberOfVideos: Int = 0
    public var numberOfAds: Int = 0
    public var numberContentPlays: Int = 0
    public var numberAdPlays: Int = 0
    public var numberErrors: Int = 0
    public var numberErrorsContent: Int = 0
    public var numberErrorsAds: Int = 0
    public var numberRequestingContent: Int = 0
    
    public var engagementDurationAds: Int = 0
    public var engagementDurationContent: Int = 0
    
    //player
    public var playerState: String = String()
    public var volumeLevelPercent: Int = 0
    
    public var seekStartPoint: Int = 0
    public var seekEndPoint: Int = 0
    public var playheadPosition: Float = Float()

    public var playerItemDuration: Float = Float()
    public var playerBufferFill: Float = Float()
    public var playerDuration: Float = Float()
    public var playerVolume: Int = Int()
    public var playerBitRate: Float = Float()
    
    // playback
    public var playbackRate: Float = Float()
    public var playbackDuration: Int = 0
    public var playbackDurationAds: Int = 0
    public var playbackDurationContent: Int = 0
    
    // time since
    public var timeSinceRequestContent: Int = 0
    public var timeSinceStartedContent: Int = 0

    public var timeSinceLastHeartbeat: Int = 0
    public var timeSinceLastMilestoneContent: Int = 0
    public var timeSinceLastStallStartContent: Int = 0
    public var timeSinceLastStallStart: Int = 0
    public var timeSinceLastBufferStartContent: Int = 0
    public var timeSinceLastBufferStart: Int = 0
    public var timeSinceLastSeekStart: Int = 0
    
    public var timeSinceLastRequestAd: Int = 0
    public var timeSinceLastAdBreakStart: Int = 0
    
    public var timeSinceAdRequested:Int = 0
    public var timeSinceAdPaused: Int = 0
    public var timeSinceAdBreakStart: Int = 0
    public var timeSinceLastAd: Int = 0
    public var timeSinceAdStarted:Int = 0
    public var timeSinceLastMilestone : Int = 0
    public var timeSinceLastMilestoneAd : Int = 0
    public var timeSinceLastAdHeartbeat : Int = 0

    public var timeSinceStallStart : Int = 0
    public var timeSinceAdStallStart : Int = 0

    public var playbackStallStart : Int = 0
    public var playbackStallAdsStart : Int = 0
    public var playbackStallContentStart : Int = 0
    public var playbackStallEnd : Int = 0
    public var playbackStallAdsEnd : Int = 0
    public var playbackStallContentEnd :  Int = 0
    
    
    public var viewStartTimestamp: Int = 0
    public var contentSessionStartTimestamp: Int = 0
    
    public var stallCount: Int = 0
    public var stallDurationContent: Int = 0
    public var stallDuration: Int = 0
    
    public var bufferDurationContent: Int = 0
    public var bufferDuration: Int = 0
    
    public var engagementDuration: Int = 0
    
    // rendition
    public var renditionName:String = String()
    public var renditionHeight: Int = 0
    public var renditionWidth: Int = 0
    public var renditionVideoBitrate: Int = 0
    
    public var frameRate: Float = Float()
    public var frameRateString: String = String()
    
    public var currentSubtitles: String = String()
    
    public var isMuted = Bool()
    public var defaultMuted = Bool()
    
    public var timeSinceLastPause: Int = 0
    public var timeSinceBufferStart: Int = 0
    public var timeSinceRequested: Int = 0
    public var timeSinceSeekStart: Int = 0
    public var timeSinceStarted: Int = 0
    public var timeSinceLastRenditionChange: Int = 0
    public var timeSinceLastStartedAd: Int = 0
    public var timeSinceLastAdCompleted: Int = 0
    
    public var milestonePercent: Float = Float()

    public var totalBufferTime: Int = 0


    var configuration: [MetricsCodingKeys] = []
}

extension DZEventDetailsMetrics : Encodable {
    enum MetricsCodingKeys: String, CodingKey, CaseIterable {
        case bandwidth = "bandwidth_kbps"
        
        case bufferDurationContent = "buffer_duration_content_ms"
        case bufferDuration = "buffer_duration_ms"
        case bufferFillPercent = "buffer_fill_percent"
        
        case contentSessionStartTimestamp = "content_session_start_ts_ms"
        case currentAudioTrack = "current_audio_track"
        case currentSubtitles = "current_subtitles"
        case engagementDuration = "engagement_duration_ms"
        case eventCount = "event_count"
        
        case numberOfVideos = "number_of_videos"
        case numberOfAds = "number_of_ads"
        case numberAdPlays = "num_ad_plays"
        case numberContentPlays = "num_content_plays"
        case numberErrors = "num_errors"
        case numberErrorsContent = "num_errors_content"
        case numberErrorsAds = "num_errors_ads"
        case numberRequestingContent = "num_requests_content"
        
        case playbackDurationAds = "playback_duration_ads_ms"
        case playbackDurationContent = "playback_duration_content_ms"
        case playbackDuration = "playback_duration_ms"
        case playbackRate = "playback_rate"
        case playerState = "player_state"
        case playerViewable = "player_viewable"
        case playerViewablePercent = "player_viewable_percent"
        case playheadPosition = "playhead_position_sec"
        
        case renditionHeight = "rendition_height"
        case renditionName = "rendition_name"
        case renditionVideoBitrate = "rendition_video_bitrate_kbps"
        case renditionWidth = "rendition_width"
        
        case stallCount = "stall_count"
        case stallCountAds = "stall_count_ads"
        case stallCountContent = "stall_count_content"
        case stallDurationContent = "stall_duration_content_ms"
        case stallDuration = "stall_duration_ms"
        case timeSinceAdBreakStart = "time_since_ad_break_start_ms"
        case timeSinceBufferStart = "time_since_buffer_start_ms"
        case timeSinceLastAdBreadStart = "time_since_last_ad_break_start_ms"
        case timeSinceLastAdCompleted = "time_since_last_ad_completed_ms"
        case timeSinceLastBufferStartContent = "time_since_last_buffer_start_content_ms"
        case timeSinceLastBufferStart = "time_since_last_buffer_start_ms"
        case timeSinceLastHeartbeat = "time_since_last_heartbeat_ms"
        case timeSinceLastMilestoneAd = "time_since_last_milestone_ad_ms"
        case timeSinceLastMilestoneContent = "time_since_last_milestone_content_ms"
        case timeSinceLastRenditionChange = "time_since_last_rendition_change_ms"
        case timeSinceLastRequestAd = "time_since_last_request_ad_ms"
        case timeSinceLastStallStartContent = "time_since_last_stall_start_content_ms"
        case timeSinceLastStallStart = "time_since_last_stall_start_ms"
        case timeSinceLastPause = "time_since_last_pause_ms"
        case timeSinceRequestAd = "time_since_request_ad_ms"
        case timeSinceRequestContent = "time_since_request_content_ms"
        case timeSinceSeekStart = "time_since_seek_start_ms"
        case timeSinceStallStartContent = "time_since_stall_start_content_ms"
        case timeSinceLastSeekStart = "time_since_last_seek_start_ms"
        case timeSinceStartedAd = "time_since_started_ad_ms"
        case timeSinceStartedContent = "time_since_started_content_ms"
        case volumeLevelPercent = "volume_level_percent"
        case timeSinceLastStartedAd = "time_since_last_started_ad_ms"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: MetricsCodingKeys.self)
        
        try container.encode(eventCount, forKey: .eventCount)
        try container.encode(numberOfVideos, forKey: .numberOfVideos)
        
//        if configuration.contains(.bandwidth) {
//            try container.encode(bandwidth, forKey: .frameRate)
//        }
        if configuration.contains(.bufferDurationContent) {
            try container.encode(bufferDurationContent, forKey: .bufferDurationContent)
        }
        if configuration.contains(.bufferDuration) {
            try container.encode(bufferDuration, forKey: .bufferDuration)
        }
//        if configuration.contains(.bufferFillPercent) {
//            try container.encode(bufferFillPercent, forKey: .bufferFillPercent)
//        }
        if configuration.contains(.contentSessionStartTimestamp) {
            try container.encode(contentSessionStartTimestamp, forKey: .contentSessionStartTimestamp)
        }
//        if configuration.contains(.currentAudioTrack) {
//            try container.encode(currentAudioTrack, forKey: .currentAudioTrack)
//        }
        if configuration.contains(.currentSubtitles) {
            try container.encode(currentSubtitles, forKey: .currentSubtitles)
        }
        if configuration.contains(.engagementDuration) {
            try container.encode(engagementDuration, forKey: .engagementDuration)
        }
        if configuration.contains(.eventCount) {
            try container.encode(eventCount, forKey: .eventCount)
        }
        if configuration.contains(.numberAdPlays) {
            try container.encode(numberAdPlays, forKey: .numberAdPlays)
        }
        if configuration.contains(.numberContentPlays) {
            try container.encode(numberContentPlays, forKey: .numberContentPlays)
        }
//        if configuration.contains(.numberErrors) {
//            try container.encode(numberErrors, forKey: .numberErrors)
//        }
//        if configuration.contains(.numberErrorsContent) {
//            try container.encode(numberErrorsContent, forKey: .numberErrorsContent)
//        }
        if configuration.contains(.numberErrorsAds) {
            try container.encode(numberErrorsAds, forKey: .numberErrorsAds)
        }
        if configuration.contains(.numberRequestingContent) {
            try container.encode(numberRequestingContent, forKey: .numberRequestingContent)
        }
        if configuration.contains(.playbackDurationAds) {
            try container.encode(playbackDurationAds, forKey: .playbackDurationAds)
        }
        if configuration.contains(.playbackDurationContent) {
            try container.encode(playbackDurationContent, forKey: .playbackDurationContent)
        }
//        if configuration.contains(.playbackDuration) {
//            try container.encode(playbackDuration, forKey: .playbackDuration)
//        }
//        if configuration.contains(.playbackRate) {
//            try container.encode(playbackRate, forKey: .playbackRate)
//        }
        if configuration.contains(.playerState) {
            try container.encode(playerState, forKey: .playerState)
        }
//        if configuration.contains(.playerViewable) {
//            try container.encode(playerViewable, forKey: .playerViewable)
//        }
//        if configuration.contains(.playerViewablePercent) {
//            try container.encode(playerViewablePercent, forKey: .playerViewablePercent)
//        }
        if configuration.contains(.playheadPosition) {
            try container.encode(playheadPosition, forKey: .playheadPosition)
        }
        if configuration.contains(.renditionHeight) {
            try container.encode(renditionHeight, forKey: .renditionHeight)
        }
        if configuration.contains(.renditionName) {
            try container.encode(renditionName, forKey: .renditionName)
        }
        if configuration.contains(.renditionVideoBitrate) {
            try container.encode(renditionVideoBitrate, forKey: .renditionVideoBitrate)
        }
        if configuration.contains(.renditionWidth) {
            try container.encode(renditionWidth, forKey: .renditionWidth)
        }
        if configuration.contains(.stallCount) {
            try container.encode(stallCount, forKey: .stallCount)
        }
//        if configuration.contains(.stallCountAds) {
//            try container.encode(stallCountAds, forKey: .stallCountAds)
//        }
//        if configuration.contains(.stallCountContent) {
//            try container.encode(stallCountContent, forKey: .stallCountContent)
//        }
        if configuration.contains(.stallDurationContent) {
            try container.encode(stallDurationContent, forKey: .stallDurationContent)
        }
        if configuration.contains(.stallDuration) {
            try container.encode(stallDuration, forKey: .stallDuration)
        }
//        if configuration.contains(.timeSinceAdBreakStart) {
//            try container.encode(timeSinceAdBreakStart, forKey: .timeSinceAdBreakStart)
//        }
//        if configuration.contains(.timeSinceBufferStart) {
//            try container.encode(timeSinceBufferStart, forKey: .timeSinceBufferStart)
//        }
//        if configuration.contains(.timeSinceLastAdBreadStart) {
//            try container.encode(timeSinceLastAdBreadStart, forKey: .timeSinceLastAdBreadStart)
//        }
        if configuration.contains(.timeSinceLastAdCompleted) {
            try container.encode(timeSinceLastAdCompleted, forKey: .timeSinceLastAdCompleted)
        }
        if configuration.contains(.timeSinceLastBufferStartContent) {
            try container.encode(timeSinceLastBufferStartContent, forKey: .timeSinceLastBufferStartContent)
        }
        if configuration.contains(.timeSinceLastBufferStart) {
            try container.encode(timeSinceLastBufferStart, forKey: .timeSinceLastBufferStart)
        }
        if configuration.contains(.timeSinceLastHeartbeat) {
            try container.encode(timeSinceLastHeartbeat, forKey: .timeSinceLastHeartbeat)
        }
        if configuration.contains(.timeSinceLastMilestoneAd) {
            try container.encode(timeSinceLastMilestoneAd, forKey: .timeSinceLastMilestoneAd)
        }
        if configuration.contains(.timeSinceLastMilestoneContent) {
            try container.encode(timeSinceLastMilestoneContent, forKey: .timeSinceLastMilestoneContent)
        }
        if configuration.contains(.timeSinceLastRenditionChange) {
            try container.encode(timeSinceLastRenditionChange, forKey: .timeSinceLastRenditionChange)
        }
        if configuration.contains(.timeSinceLastRequestAd) {
            try container.encode(timeSinceLastRequestAd, forKey: .timeSinceLastRequestAd)
        }
        if configuration.contains(.timeSinceLastStallStartContent) {
            try container.encode(timeSinceLastStallStartContent, forKey: .timeSinceLastStallStartContent)
        }
        if configuration.contains(.timeSinceLastStallStart) {
            try container.encode(timeSinceLastStallStart, forKey: .timeSinceLastStallStart)
        }
        if configuration.contains(.timeSinceLastPause) {
            try container.encode(timeSinceLastPause, forKey: .timeSinceLastPause)
        }
//        if configuration.contains(.timeSinceRequestAd) {
//            try container.encode(timeSinceRequestAd, forKey: .timeSinceRequestAd)
//        }
        if configuration.contains(.timeSinceRequestContent) {
            try container.encode(timeSinceRequestContent, forKey: .timeSinceRequestContent)
        }
//        if configuration.contains(.timeSinceSeekStart) {
//            try container.encode(timeSinceSeekStart, forKey: .timeSinceSeekStart)
//        }
        if configuration.contains(.timeSinceLastSeekStart) {
            try container.encode(timeSinceLastSeekStart, forKey: .timeSinceLastSeekStart)
        }
//        if configuration.contains(.timeSinceStallStartContent) {
//            try container.encode(timeSinceStallStartContent, forKey: .timeSinceStallStartContent)
//        }
//        if configuration.contains(.timeSinceStartedAd) {
//            try container.encode(timeSinceStartedAd, forKey: .timeSinceStartedAd)
//        }
        if configuration.contains(.timeSinceStartedContent) {
            try container.encode(timeSinceStartedContent, forKey: .timeSinceStartedContent)
        }
//        if configuration.contains(.volumeLevelPercent) {
//            try container.encode(volumeLevelPercent, forKey: .volumeLevelPercent)
//        }
        
        if configuration.contains(.timeSinceLastStartedAd) {
            try container.encode(timeSinceLastStartedAd, forKey: .timeSinceLastStartedAd)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        MetricsCodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}

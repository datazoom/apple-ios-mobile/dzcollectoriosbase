//
//  DZEventDevice.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
import UIKit
import AdSupport

#if canImport(AppTrackingTransparency)
import AppTrackingTransparency
#endif

public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

public class DZEventDevice {
    public var deviceId: String
    public var deviceName: String
    public var deviceManufacturer: String
    public var deviceType: String

    public var systemName: String
    public var systemVersion: String
    public var systemModel: String
    
    public var userAgent: String = String()
    
    public var advertisingId: String
    public var advertisingTracking: Bool
    public var advertisingTrackingOptOut: Bool
    
    public var ScreenWidth: Int = Int()
    public var ScreenHeight: Int = Int()
    
    var configuration: [CodingKeys] = []
    
    public init() {
        self.deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
        self.deviceName = UIDevice.current.name
        
        self.systemName = UIDevice.current.systemName
        self.systemVersion = UIDevice.current.systemVersion
        self.systemModel = UIDevice.current.modelName
        self.userAgent = "\(systemModel) \(systemName) \(systemVersion)"
        
        self.ScreenWidth = Int(UIScreen.main.nativeBounds.size.width)
        self.ScreenHeight = Int(UIScreen.main.nativeBounds.size.height)
        
        self.deviceManufacturer = "Apple"
        self.deviceType = "ott device"
        self.userAgent = "\(systemModel) \(systemName) \(systemVersion)"
        
        self.advertisingId = DZEventDevice.getIDFA()
        self.advertisingTracking = DZEventDevice.isAdTrackingEnabled()
        self.advertisingTrackingOptOut = DZEventDevice.isAdTrackingOptOut()
    }
}

extension DZEventDevice {
    //MARK:- Get DeviceId
    private class func getDeviceId() -> String?{
        return UUID().uuidString
    }
    
    private class func getIDFA() -> String {
        if #available(iOS 14, *) {
            if ATTrackingManager.trackingAuthorizationStatus == .authorized {
                // Authorized
                let idfa = ASIdentifierManager.shared().advertisingIdentifier
                return idfa.uuidString
            }
            else {
                return "00000000-0000-0000-0000-000000000000"
            }
        }
        else if #available(macOS 10.14, *) {
            guard ASIdentifierManager.shared().isAdvertisingTrackingEnabled else {
                return "00000000-0000-0000-0000-000000000000"
            }

            let idfa = ASIdentifierManager.shared().advertisingIdentifier
            return idfa.uuidString
        }
        else {
            return "00000000-0000-0000-0000-000000000000"
        }
    }

    private class func isAdTrackingEnabled() -> Bool {
        if #available(iOS 14, *) {
            return ATTrackingManager.trackingAuthorizationStatus == .authorized
        }
        else if #available(macOS 10.14, *) {
            return ASIdentifierManager.shared().isAdvertisingTrackingEnabled
        }
        else {
            return false
        }
    }
    
    private class func isAdTrackingOptOut() -> Bool {
//        if UserDefaults.standard.isTrackingAuthorized() {
//            if #available(tvOS 14, *) {
//                return (ATTrackingManager.trackingAuthorizationStatus == .denied)
//            }
//            else {
//                return !ASIdentifierManager.shared().isAdvertisingTrackingEnabled
//            }
//        }
//        else {
//            return false
//        }
        return UserDefaults.standard.isTrackingAuthorizationOptOut()
    }
}

extension DZEventDevice : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case deviceId = "device_id"
        case deviceManufacturer = "device_mfg"
        case deviceType = "device_type"
        case deviceName = "device_name"
        
        case systemName = "os_name"
        case systemVersion = "os_version"
        case systemModel = "system_model"
        
        case userAgent = "user_agent"
        
        case advertisingId = "advertising_id"
        case advertisingTracking = "ad_tracker_opt_out"
        case advertisingTrackingOptOut = "ad_tracking_opt_out"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.deviceId) {
            try container.encode(deviceId, forKey: .deviceId)
        }
        if configuration.contains(.deviceManufacturer) {
            try container.encode(deviceManufacturer, forKey: .deviceManufacturer)
        }
        if configuration.contains(.deviceType) {
            try container.encode(deviceType, forKey: .deviceType)
        }
        if configuration.contains(.deviceName) {
            try container.encode(deviceName, forKey: .deviceName)
        }
        if configuration.contains(.userAgent) {
            try container.encode(userAgent, forKey: .userAgent)
        }
        if configuration.contains(.systemName) {
            try container.encode(systemName, forKey: .systemName)
        }
        if configuration.contains(.systemVersion) {
            try container.encode(systemVersion, forKey: .systemVersion)
        }
        if configuration.contains(.advertisingId) {
            try container.encode(advertisingId, forKey: .advertisingId)
        }
        if configuration.contains(.advertisingTracking) {
            try container.encode(advertisingTracking, forKey: .advertisingTracking)
        }
        if configuration.contains(.advertisingTrackingOptOut) {
            try container.encode(advertisingTrackingOptOut, forKey: .advertisingTrackingOptOut)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}

//
//  DZEventDetails.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
import UIKit

//MARK:- DZEventDetails - event details, attributes and metrics
public class DZEventDetails {
    public var type: EventType = EventType.none
    public var timestamp: Int = 0
    
    public var metrics: DZEventDetailsMetrics = DZEventDetailsMetrics()
    public var attributes: DZEventDetailsAttributes = DZEventDetailsAttributes()
    
    var configuration: [EventCodingKeys] = []
    
    public init() {
        self.type = EventType.none
        self.timestamp = 0
        
        self.metrics = DZEventDetailsMetrics()
        self.attributes = DZEventDetailsAttributes()
    }
    
    public convenience init(_ type: EventType) {
        self.init()
        self.type = type
    }
}

extension DZEventDetails : Encodable {
    enum EventCodingKeys: String, CodingKey, CaseIterable {
        case type = "type"
        case timestamp = "timestamp"
        case milestonePercent = "milestone_percent"
        case metrics = "metrics"
        case attributes = "attributes"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: EventCodingKeys.self)
        try container.encode(type.key, forKey: .type)
        try container.encode(timestamp, forKey: .timestamp)
        
        if (type == .error || type == .milestone || type == .renditionChange || type == .seekEnd  || type == .playbackStart) {
            print("Add attributes for \(type.key) \(attributes.configuration)")
            try container.encode(attributes, forKey: .attributes)
        }

        try container.encode(metrics, forKey: .metrics)
    }
    
    public func initWithConfiguration(type: EventType, mediaType: VideoType, meta: [String], flux: [String]){
        configuration = []
        EventCodingKeys.allCases.forEach { item in
            if meta.contains(item.stringValue) {
                configuration.append(item)
            }
        }
        
        metrics.initWithConfiguration(flux)
        attributes.initWithConfiguration(type: self.type, mediaType: mediaType, from: meta)
    }
}

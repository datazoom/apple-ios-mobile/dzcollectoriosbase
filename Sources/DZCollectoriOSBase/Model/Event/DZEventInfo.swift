//
//  DZEventInfo.swift
//  GoldCollectorTvOS
//
//  Created by Momcilo Stankovic on 03/04/2021.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
class DZEventInfo {

    var eventId = String()
    var eventName = String()
    var isSeek      = Bool()
    var isSeeked    = Bool()
    var name        = "NA"
    var version     = "NA"
    var player_id   = String()
    var device_id   = String()

    public var connectionURL                = String()
    
    public var sessionViewID                = String()
    public var dzSdkVersion                = String()

    
    public var oAuthToken                   = String()
    public var customerCode                 = String()
    public var connectorList                = String()
    public var configurationId              = String()
    
    public var os                           = String()
    public var deviceName                   = String()
    public var systemName                   = String()
    public var systemModel                  = String()
    public var systemVersion                = String()
    public var internetServiceProvider      = String()
    public var orientation                  = String()
    
    public var asset_id                     = String()
    public var user_agent                   = String()
    public var player_state                 = String()
    public var player_volume                = Int()
    public var player_bitRate               = Float()
    public var playback_rate                = Float()
    public var play_head_position           = Float()
    public var player_item_duration         = Float()
    public var player_buffer_fill           = Float()
    
    public var toMilliss                    = Float64()
    public var fromMilliss                  = Float64()
    public var eventList                    = [String]()
    
    public var isMuted                      = Bool()
    public var autoStart                    = Bool()
    public var controls                     = Bool()
    public var defaultPlaybackRate          = Float()
    public var loop                         = Bool()
    public var fullscreen                   = Bool()
    public var readyState                   = String()
    public var streamingProtocol            = String()
    public var streamingType                = String()
    public var numberOfVideos               = Int()
    public var numberOfErrors               = Int()
    public var defaultMuted                 = Bool()

    
    public var timeSinceBufferStart         = Int()
    public var timeSincePaused              = Int()
    public var timeSinceRequested           = Int()
    public var timeSinceSeekStart           = Int()
    public var timeSinceStarted             = Int()
    public var timeSinceLastRenditionChange = Int()
    
    public var renditionName = String()
    public var renditionHeight = Int()
    public var renditionWidth = Int()
    public var milestonePercent   :Float      = 0.0

    public var totalBufferTime = Int()

    public var unsentJsonObjectArray        = [[String: Any]]()
    
    public var asn              = String()
    public var city             = String()
    public var country          = String()
    public var countryCode      = String()
    public var isp              = String()
    public var latitude         = Double()
    public var longtitude       = Double()
    public var org              = String()
    public var query            = String()
    public var region           = String()
    public var regionName       = String()
    public var zip              = String()
    public var connectionType   = String()
    
    var customMetadata   = [String:Any]()
    
    public var adData = [String: Any]()
    
    public var playerHeight = Int()
    public var playerWidth = Int()
    public var title = String()
    
    public var errorCode = Int()
    public var errorMsg = String()
    
    public var videoType = String()
    
    public var frameRate = Double()
    
    public var timeSinceAdRequested:Int = 0
    public var timeSinceAdStarted:Int = 0
    public var timeSinceAdPaused: Int = 0
    public var timeSinceAdBreakStart: Int = 0
    public var timeSinceLastAd: Int = 0
    public var timeSinceLastMilestone : Int = 0
    public var timeSinceLastAdMilestone : Int = 0
    public var timeSinceLastHeartbeat : Int = 0
    public var timeSinceLastAdHeartbeat : Int = 0
    public var timeSinceAdStallStart : Int = 0
    public var timeSinceStallStart : Int = 0
    public var playbackStallStart : Int = 0
    public var playbackStallAdsStart : Int = 0
    public var playbackStallContentStart : Int = 0
    public var playbackStallEnd : Int = 0
    public var playbackStallAdsEnd : Int = 0
    public var playbackStallContentEnd :  Int = 0
    
    public var playbackDurationAds: Int = 0
    public var playbackDurationContent: Int = 0
    public var engagementDuration = 0
    public var viewStartTimestamp = 0
    public var renditionVideoBitrate = 0
    public var playerViewablePercent = 0
    public var numberOfAds = 0
    public var timeSinceAdBufferStart = 0
    
    public var extraData = [String: [String: Any]]()

    
    init() {
        
    }
}

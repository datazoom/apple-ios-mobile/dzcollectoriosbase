//
//  DZBaseWebBroker.swift
//  DZCollectoriOSBase
//
//  Created by Momcilo Stankovic on 05/04/21.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
import UIKit
import Reachability

typealias ServiceResponse = (Data, Error?) -> Void

open class DZBaseWebBroker : NSObject {
    let reachability = try! Reachability()
    
    //MARK:- Get Response Form API
    func getTheAPIToWork(method: String, connectionURL:String, onCompletion: @escaping (Data?, Error?) -> Void) {
        if reachability.connection != .unavailable {
            let route = connectionURL //API.mainURL
            makeHTTPGetRequest(path: route, onCompletion: { (response, error) in
                if error == nil {
                    onCompletion(response ,nil)
                } else {
                    onCompletion(nil ,error)
                }
            })
        }
    }
    
    
    //MARK:- Get Request
    func makeHTTPGetRequest(path: String, onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if data != nil  {
                if (data!.count > 0){
                    onCompletion(data!, error)
                }
                else {
                    let err = DZEventResponseError.notFound
                    onCompletion(data!, err)
                }
            }
        })
        task.resume()
    }
    
    //MARK:- Post Request
    func makeHTTPPostRequest(path: String, body: [String: Any], onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        // Set the method to POST
        request.httpMethod = "POST"
        let payload = try? JSONSerialization.data(withJSONObject: body)
        
        // Set the POST body for the request
        request.setValue("application/json", forHTTPHeaderField: "content-type")

        request.httpBody =  payload
        let session = URLSession.shared
        let task = session.dataTask(with: (request as URLRequest) as URLRequest, completionHandler: {data, response, error -> Void in
            if data != nil  {
                onCompletion(data!, error)
            }
        })
        task.resume()
    }
    
    func sendPostRequest(path: String, body: String, onCompletion: @escaping ServiceResponse) {
        let payload = body.data(using: .utf8)
        let request = NSMutableURLRequest(url: NSURL(string: path)! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        request.httpBody =  payload
        
        let session = URLSession.shared
        let task = session.dataTask(with: (request as URLRequest) as URLRequest, completionHandler: {data, response, error -> Void in
            if data != nil  {
                onCompletion(data!, error)
            }
        })
        task.resume()
    }
    
    //MARK:- Get Response Form API
    func getThePostAPIToWork(method: String, parameter: [String : Any], connectionURL:String, onCompletion: @escaping (Data?, Error?) -> Void) {
        if reachability.connection != .unavailable {
            let route = connectionURL
            makeHTTPPostRequest(path: route, body: parameter) { (response, error) in
                onCompletion(response ,error)
            }
        } else {
            print("%%%% Network not reachable - apiToWork %%%% \n")
        }
    }
    
    func sendEventToAPI(method: String, parameter: String, connectionURL:String, onCompletion: @escaping (Data?, Error?) -> Void) {
        if reachability.connection != .unavailable {
            let route = connectionURL //API.mainURL
            sendPostRequest(path: route, body: parameter) { (response, error) in
                onCompletion(response ,error)
            }
        } else {
            print("%%%% Network not reachable - sendEventToAPI %%%% \n")
        }
    }
    
    
    @objc func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        print ("Reachability connection in BaseWebBroker: \(reachability.connection) %%%% \n")
        if reachability.connection != .unavailable {
//            print("===============================================================Reachable via WiFi")
        } else {
            print("%%%% Network not reachable - Reachability changed %%%% \n")
        }
    }
}
